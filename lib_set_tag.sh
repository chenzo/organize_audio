#!/bin/bash

###
# Author: Vincent Lucas <vincent.lucas@gmail.com>
###

function set_tag_usage
{
    echo "Usage:";
    echo "    set_tag filename";
    echo "    <-genre> <-artist> <-album> <-title> <-tracknumber> <-date>";
}

function set_tag
{
    if [ $# -eq 0 ]
    then
        set_tag_usage;
        return 1;
    fi

    filename="$1";
    shift;

    options=`getopt -a --long genre:,artist:,album:,title:,tracknumber:,date: \
        -- "" "$@"`;
    eval set -- "$options";

    if [ $? -ne 0 ]
    then
        set_tag_usage;
        return 1;
    fi

    tags=`vorbiscomment -l "$filename" \
        | grep -E "^((genre)|(artist)|(album)|(title)|(tracknumber)|(date))" \
        | sort`;

    while [ "$1" != "--" ]
    do
        key=`echo "$1" | tr -d "-"`;
        value="$2";

        tags=`echo "${tags}" | sed "/^${key}.*/d"`;
        tags=`echo -e "${tags}\n${key}=${value}"`;

        shift 2;
    done

    echo "${tags}" | vorbiscomment -w -c - "${filename}";
}

#set_tag "$@";
