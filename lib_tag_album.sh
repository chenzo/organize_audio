#!/bin/bash

###
# Author: Vincent Lucas <vincent.lucas@gmail.com>
###

source lib_set_tag.sh;

function tag_album_usage
{
    echo "Usage:";
    echo "    $0 directory";
    exit 1;
}

function tag_album
{
    if [ $# -eq 0 ]
    then
        usage;
    fi

    dir="$1";

    f=`find ${dir}/* | head -n 1`;
    tmp_dir=`dirname "$f"`;
    dir_album=`basename "${tmp_dir}"`;
    tmp_dir=`dirname "${tmp_dir}"`;
    dir_artist=`basename "${tmp_dir}"`;
    tmp_dir=`dirname "${tmp_dir}"`;
    dir_genre=`basename "${tmp_dir}"`;

    # Read genre
    genre_tag=`vorbiscomment -l "$f" | grep -E "^genre" | cut -d "=" -f 2`;
    if [ "${genre_tag}" == "" ]
    then
        genre_tag="${dir_genre}";
    fi
    read -e -p "genre: " -i "$genre_tag" genre;

    # Read artist
    artist_tag=`vorbiscomment -l "$f" | grep -E "^artist" | cut -d "=" -f 2`;
    if [ "${artist_tag}" == "" ]
    then
        artist_tag="${dir_artist}";
    fi
    read -e -p "artist: " -i "$artist_tag" artist;

    # Read album
    album_tag=`vorbiscomment -l "$f" | grep -E "^album" | cut -d "=" -f 2`;
    if [ "${album_tag}" == "" ]
    then
        album_tag="${dir_album}";
    fi
    read -e -p "album: " -i "$album_tag" album;

    # Read date
    date_tag=`vorbiscomment -l "$f" | grep -E "^date" | cut -d "=" -f 2`;
    read -e -p "date: " -i "$date_tag" date;

    # Loop over the file in order to set title and tracknumber.
    nb=0;
    for f in ${dir}/*
    do
        echo -e "\nfile: $f";

        # Read tracknumber
        nb=`expr $nb + 1`;
        printf -v nb_str '%02d' $nb;
        read -e -p "tracknumber: " -i "$nb_str" tracknumber;
        nb=${tracknumber};

        # Read title
        title_tag=`vorbiscomment -l "$f" | grep -E "^title" | cut -d "=" -f 2`;
        if [ "${title_tag}" == "" ]
        then
            title_tag=`basename "${f}"`;
        fi
        read -e -p "title: " -i "$title_tag" title;

        # Set the tags
        set_tag "${f}" \
            -album "${album}" \
            -artist "${artist}" \
            -date "${date}" \
            -genre "${genre}" \
            -title "${title}" \
            -tracknumber "${tracknumber}";
    done
}

#main $@;
