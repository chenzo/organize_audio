#!/bin/bash

###
# Author: Vincent Lucas <vincent.lucas@gmail.com>
###

source lib_tag_album.sh;

function rip_usage
{
    echo "Usage:";
    echo "    ./rip.sh <genre> <artist> <album> [<--no-rip>]";
}

function rip
{
    if [ $# -ne 1 ]
    then
        return 1;
    fi

    dir=$1;

    cdparanoia -B;
    mv track* ${dir}/.

    return $?;
}

function create_dir
{
    if [ $# -ne 1 ]
    then
        return 1;
    fi

    dir=$1;
    mkdir -p ${dir};

    return 0;
}

function convert_to_ogg
{
    if [ $# -ne 1 ]
    then
        return 1;
    fi

    dir=$1;
    dir2ogg -w --delete-input -d ${dir}/*;

    return $?;
}

function rename_ogg_files
{
    if [ $# -ne 1 ]
    then
        return 1;
    fi

    dir=$1;
    for filename in `ls ${dir}/*`
    do
        tracknumber=`vorbiscomment -l "$filename" \
            | grep -E "^tracknumber" \
            | cut -d "=" -f 2`;
        title=`vorbiscomment -l "$filename" \
            | grep -E "^title" \
            | cut -d "=" -f 2`;

        title=`echo ${title} \
            | sed -r \
                -e 's/[^A-Za-Z0-9]/_/g' \
                -e 's/[àäâ]/a/g' \
                -e 's/[éèëê]/e/g' \
                -e 's/[ïî]/i/g' \
                -e 's/[öô]/o/g' \
                -e 's/[ùüû]/u/g' \
                -e 's/[ç]/c/g' \
        `;

        new_filename="${dir}/${tracknumber}_${title}.ogg";

        mv ${filename} ${new_filename};
    done
}

function mp3_convert
{
    if [ $# -ne 2 ]
    then
        return 1;
    fi

    ogg_dir=$1;
    mp3_dir=$2;
    for ogg_filename_with_path in `ls ${ogg_dir}/*`
    do
        ogg_filename=`basename ${ogg_filename_with_path}`;
        mp3_filename=`echo "${ogg_filename}" \
            | rev \
            | cut -d "." -f 2- \
            | rev`.mp3;
        mp3_filename_with_path="${mp3_dir}/${mp3_filename}";
        echo -e "\n$ogg_filename_with_path -> $mp3_filename_with_path";
        avconv -i "${ogg_filename_with_path}" -map_metadata 0:s:0 "${mp3_filename_with_path}";
    done
}

function main
{
    if [ $# -lt 3 ]
    then
        rip_usage;
        return 1;
    fi

    genre="$1";
    artist="$2";
    album="$3";
    ogg_base_dir="./music/ogg";
    ogg_dir="${ogg_base_dir}/${genre}/${artist}/${album}";
    mp3_base_dir="./music/mp3";
    mp3_dir="${mp3_base_dir}/${genre}/${artist}/${album}";
    do_rip=1;
    shift 3;

    # Parse arguments
    options=`getopt -a --long no-rip \
        -- "" "$@"`;
    eval set -- "$options";

    if [ $? -ne 0 ]
    then
        rip_usage;
        return 1;
    fi

    while [ "$1" != "--" ]
    do
        if [ "$1" == "--no-rip" ]
        then
            do_rip=0;
        fi
        shift 1;
    done

    create_dir ${ogg_dir};
    create_dir ${mp3_dir};
    if [ $do_rip -eq 1 ]
    then
        rip ${ogg_dir};
    fi
    convert_to_ogg ${ogg_dir};
    tag_album ${ogg_dir};
    rename_ogg_files ${ogg_dir};
    mp3_convert ${ogg_dir} ${mp3_dir};
}

main $@;
